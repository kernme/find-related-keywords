---------------------------------------------
-- Insert Item into a List
---------------------------------------------
on insertItemInList(theItem, theList, thePosition)
	set theListCount to length of theList
	if thePosition is 0 then
		return false
	else if thePosition is less than 0 then
		if (thePosition * -1) is greater than theListCount + 1 then return false
	else
		if thePosition is greater than theListCount + 1 then return false
	end if
	if thePosition is less than 0 then
		if (thePosition * -1) is theListCount + 1 then
			set beginning of theList to theItem
		else
			set theList to reverse of theList
			set thePosition to (thePosition * -1)
			if thePosition is 1 then
				set beginning of theList to theItem
			else if thePosition is (theListCount + 1) then
				set end of theList to theItem
			else
				set theList to (items 1 thru (thePosition - 1) of theList) & theItem & (items thePosition thru -1 of theList)
			end if
			set theList to reverse of theList
		end if
	else
		if thePosition is 1 then
			set beginning of theList to theItem
		else if thePosition is (theListCount + 1) then
			set end of theList to theItem
		else
			set theList to (items 1 thru (thePosition - 1) of theList) & theItem & (items thePosition thru -1 of theList)
		end if
	end if
	return theList
end insertItemInList

---------------------------------------------
-- Make a list from an existing file
---------------------------------------------
on makeListFromFile(theFile)
	set theList to {}
	set theLines to paragraphs of (read POSIX file theFile)
	repeat with nextLine in theLines
		if length of nextLine is greater than 0 then
			copy nextLine to the end of theList
		end if
	end repeat
	return theList
end makeListFromFile

---------------------------------------------
-- Click the search button
---------------------------------------------
on activateSearchButton()
	set searchButtonPath to "#gnav-search > div > div.search-button-wrapper.hide > button"
	tell application "Safari"
		do JavaScript "document.querySelector('" & searchButtonPath & "').click();" in document 1
	end tell
end activateSearchButton

---------------------------------------------
-- Set the Input
---------------------------------------------
on setInput(keyword)
	tell application "System Events"
		tell process "Safari"
			set value of text field 1 of group 1 of group 1 of group 1 of group 2 of UI element 1 of scroll area 1 of group 1 of group 1 of tab group 1 of splitter group 1 of window 1 to keyword
		end tell
	end tell
end setInput

---------------------------------------------
-- Find Etsy's Related Keywords
---------------------------------------------
on getRelatedTagsFromDOM(instance)
	set relatedTags_DOMPath to "#content .guided-search li:nth-child"
	tell application "Safari"
		do JavaScript "document.querySelector('" & relatedTags_DOMPath & "(" & instance & ") a').innerText;" in document 1
	end tell
end getRelatedTagsFromDOM

---------------------------------------------
-- Find Related Keywords from Existing File
---------------------------------------------
on findRelatedTags()
	set theList to makeListFromFile(file_baseKeywords)
	repeat with a from 1 to length of theList
		
		set theCurrentListItem to item a of theList
		setInput(theCurrentListItem)
		activateSearchButton()
		waitForPageLoad()
		loopRelatedTags()
	end repeat
end findRelatedTags

---------------------------------------------
-- Loop through the related tags
---------------------------------------------
on loopRelatedTags()
	set theList to {}
	set text item delimiters to ","
	set theCount to 0
	
	repeat
		set updatedCount to (theCount + 1)
		set theData to getRelatedTagsFromDOM(updatedCount)
		
		if theData is false then
			exit repeat
		end if
		
		insertItemInList(theData & newLine, theList, 1)
		
		set theCount to theCount + 1
	end repeat
	
	return theList
end loopRelatedTags